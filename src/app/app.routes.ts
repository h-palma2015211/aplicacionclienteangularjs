import {RouterModule, Routes} from '@angular/router';
import { AuntenticarComponent } from './component/auntenticar/auntenticar.component';
import { UsuarioComponent } from './component/usuario/usuario.component';
import { HomeComponent } from './component/home/home.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { DashboardALComponent } from './component/dashboard-al/dashboard-al.component';
import { DashboardPComponent } from './component/dashboard-p/dashboard-p.component';
import { dashboard_routes } from './component/dashboard/dashboard.routes';

const APP_ROUTES:Routes = [
    {path: 'login', component: AuntenticarComponent},
    {path: 'administrador', component: DashboardComponent, 
    children: dashboard_routes},
    {path: 'profesor', component: DashboardPComponent},
    {path: 'alumno', component: DashboardALComponent},
    {path: '**', pathMatch:'full', redirectTo:'login'}

];
export const app_routing = RouterModule.forRoot(APP_ROUTES);