import { AsignarPComponent } from './component/dashboard/profesor/asignarP-form';
import { NavbarAComponent } from './component/dashboard/navbarA/navbarA.component';
import { UsuarioService } from './servicios/usuario.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {PeticionesServices} from './servicios/peticiones.service';
import {HttpModule,  Response, Headers} from '@angular/http';
import { NavbarComponent } from './component/navbar/navbar.component';
import { AuntenticarComponent } from './component/auntenticar/auntenticar.component';
import { HomeComponent } from './component/home/home.component';
import { UsuarioComponent } from './component/usuario/usuario.component';
///////////////
import { app_routing } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { DashboardPComponent } from './component/dashboard-p/dashboard-p.component';
import { DashboardALComponent } from './component/dashboard-al/dashboard-al.component';
import { AuthGuardService } from './servicios/auth.service';
import { NavbarALComponent } from './component/dashboard-al/navbarAL/navbarAL.component';
import { NavbarPComponent } from './component/dashboard-p/navbarP/navbarP.component';
import { ProfesorComponent } from './component/dashboard/profesor/profesor.component';
import { ProfesorFormComponent } from './component/dashboard/profesor/profesor-form.component';
import { ProfesorService } from './servicios/profesor.service';
import { MateriaComponent } from './component/dashboard/materia/materia.component';
import { MateriaService } from './servicios/materia.service';
import { MateriaFormComponent } from './component/dashboard/materia/materia-form';
import { MateriaAsinacionComponent } from './component/dashboard-al/materia-asinacion/materia-asinacion.component';
import { AsignacionesComponent } from './component/dashboard/profesor/asignaciones.component';
import { AsignacionesMComponent } from './component/dashboard/asignaciones-m/asignaciones-m.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AuntenticarComponent,
    HomeComponent,
    UsuarioComponent,
    DashboardComponent,
    DashboardPComponent,
    DashboardALComponent,
    NavbarAComponent,
    NavbarALComponent,
    NavbarPComponent,
    ProfesorComponent,
    ProfesorFormComponent,
    MateriaComponent,
    MateriaFormComponent,
    MateriaAsinacionComponent,
    AsignarPComponent,
    AsignacionesMComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
     app_routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthGuardService,
    UsuarioService,
    ProfesorService,
    MateriaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
