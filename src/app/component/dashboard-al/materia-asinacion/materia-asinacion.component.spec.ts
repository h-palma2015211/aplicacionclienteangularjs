import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MateriaAsinacionComponent } from './materia-asinacion.component';

describe('MateriaAsinacionComponent', () => {
  let component: MateriaAsinacionComponent;
  let fixture: ComponentFixture<MateriaAsinacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriaAsinacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MateriaAsinacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
