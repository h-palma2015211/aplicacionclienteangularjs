import { Component, OnInit } from '@angular/core';
import { MateriaService } from '../../../servicios/materia.service';

@Component({
  selector: 'app-materia-asinacion',
  templateUrl: './materia-asinacion.component.html',
  styleUrls: ['./materia-asinacion.component.css']
})
export class MateriaAsinacionComponent implements OnInit {
  materias:any[] = [];
  constructor(private materiaService: MateriaService ) { }

  ngOnInit() {
  }
  private inicializar() {
    this.materiaService.getMaterias().subscribe( res => {
      this.materias = res;
    });
  }

}
