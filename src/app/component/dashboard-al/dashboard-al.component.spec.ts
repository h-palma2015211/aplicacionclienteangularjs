import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardALComponent } from './dashboard-al.component';

describe('DashboardALComponent', () => {
  let component: DashboardALComponent;
  let fixture: ComponentFixture<DashboardALComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardALComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardALComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
