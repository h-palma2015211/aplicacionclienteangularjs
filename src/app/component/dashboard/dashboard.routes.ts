import { AsignarPComponent } from './profesor/asignarP-form';
import { MateriaComponent } from './materia/materia.component';
import { DashboardComponent } from './dashboard.component';
import { Routes } from '@angular/router';
import { ProfesorComponent } from './profesor/profesor.component';
import { ProfesorFormComponent } from './profesor/profesor-form.component';
import { MateriaFormComponent } from './materia/materia-form';
import { AsignacionesMComponent } from './asignaciones-m/asignaciones-m.component';
export const dashboard_routes:Routes = [
  { path: 'profesor', component: ProfesorComponent },
  { path: 'profesor/:idProfesor', component:  ProfesorFormComponent},
  { path: 'materia', component: MateriaComponent},
  {path: 'materia/:idMateria', component: MateriaFormComponent},
  {path: 'asignaciones', component: AsignacionesMComponent},
  {path: 'asignar/:idProfesor', component: AsignarPComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard'}
]