import { MateriaService } from '../../../servicios/materia.service';
import { ProfesorService } from '../../../servicios/profesor.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-asignarP',
  templateUrl: './asignarP-form.html'
})
export class AsignarPComponent {
 

  
    url:string;
    asignacion: any = {
        idMateria: 0,
        idSeccion: 0
    }
    seccion : any[] = [];
    materia: any[] = [];

    notificacion:any = {
    estado:false,
    mensaje: ""
    }
    constructor( private router:Router,
    private activatedRoute: ActivatedRoute,
    private profesorService: ProfesorService,
    private materiaProfesor: MateriaService
  ) 
    {
      
      this.inicializar1();
      this.inicializar2();
      this.activatedRoute.params.subscribe(params => {
      this.url = params["idProfesor"];
      
      console.log(this.url);
      this.asignacion.idProfesor = this.url;
      console.log(this.asignacion.idProfesor);
      /*this.profesorService.getProfesor(this.url)
      .subscribe(c =>  this.profesors = c);
      */
      
       
    });
    }
    private inicializar2(){
      this.materiaProfesor.getMaterias()
      .subscribe(res => {
        this.materia = res;
      });
    }

    private inicializar1(){
      this.profesorService.getSecciones()
      .subscribe(res => {
        this.seccion = res;
      });
    }


    guardarCambios() {

        console.log(this.asignacion);

       this.profesorService.nuevaAsignacion(this.asignacion)
       .subscribe(res => {
          console.log(res);
           this.router.navigate(['/administrador/profesor']);
       })
            /* this.profesorService.editarProfesor(this.profesors, this.url)
      .subscribe(res => {
        console.log(res);
        /*this.notificacion.estado = true;
        this.notificacion.mensaje = "Se modifico Profesor";
        setTimeout(() => {
      this.resetNotificacion();
    }, 2000);
      this.router.navigate(['/administrador/profesor']);
      });*/
    }

    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }

}