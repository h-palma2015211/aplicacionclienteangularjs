import { ProfesorService } from '../../../servicios/profesor.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-asignaciones-m',
  templateUrl: './asignaciones-m.component.html',
  styleUrls: ['./asignaciones-m.component.css']
})
export class AsignacionesMComponent implements OnInit {
  asignaciones: any[];
  constructor(public profesorService: ProfesorService) { }

  ngOnInit() {
    this.inicializar();
  }

  public inicializar() {
    this.profesorService.getAsignaciones()
    .subscribe(
        res => {
          this.asignaciones = res[0];
        }
    );
  }

}
