import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionesMComponent } from './asignaciones-m.component';

describe('AsignacionesMComponent', () => {
  let component: AsignacionesMComponent;
  let fixture: ComponentFixture<AsignacionesMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignacionesMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionesMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
