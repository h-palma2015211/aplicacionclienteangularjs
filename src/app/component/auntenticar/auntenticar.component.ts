import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../servicios/usuario.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-auntenticar',
  templateUrl: './auntenticar.component.html'
})
export class AuntenticarComponent implements OnInit {
  formularioLogin: FormGroup;
  constructor(  private usuarioSevice: UsuarioService) { }

  ngOnInit() {
        let validaciones = [Validators.required,  Validators.minLength(6)];

    this.formularioLogin = new FormGroup({
      'nick': new FormControl('', validaciones),
      'contrasena': new FormControl('', validaciones)
    });

  }


   public iniciarSesion() {
      console.log(this.formularioLogin.value);
      this.usuarioSevice.auntenticar(this.formularioLogin.value);
   }


}
