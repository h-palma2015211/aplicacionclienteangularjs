import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
@Injectable()
export class UsuarioService{
    

    
    constructor(private router: Router,
        private _http: Http) {

    }


    public auntenticar(usuario:any) {
        
        let uri = 'http://localhost:3000/auth/';
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});
        let data = JSON.stringify(usuario);

            this._http.post(uri, data, options)
            .subscribe( res => {
                
                this.obtenerTipo(res.json().idUsuario)
                .subscribe(
                    res => {
                        console.log(res.idTipoUsuario);
                       if(res.idTipoUsuario == 1){
                            this.router.navigate(['/administrador']);
                       } 
                        if(res.idTipoUsuario == 2) {
                           this.router.navigate(['/profesor']);

                        }
                        if(res.idTipoUsuario == 3){
                         this.router.navigate(['/alumno']);
 
                        }
                    }
                );
                this.setToken(res.json().token);
                this.setCurrentUser({
                    nick: res.json().nick,
                    idUsuario: res.json().idUsuario
                });
                
                
             },
                        error => {
                            console.log(error.text());
                         });  
    }


    public getToken():string {
    return localStorage.getItem('token');
  }

    public obtenerTipo(id:number){
        let uri = 'http://localhost:3000/auth/';
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers})
        let u = uri + id;
         return this._http.get(u, options)
        .map(
            res => {
                return res.json();
                }
        );
    }

    public setToken (token: string) {
        if(localStorage.getItem('token') != token) {
            localStorage.removeItem('token');
            localStorage.setItem('token', token);
            localStorage.setItem('token', token);
    
        } 
    }

    public setCurrentUser(usuario: any) {
        localStorage.setItem('user', JSON.stringify(usuario));
    }

    public verificarSesion(): boolean{
        if(localStorage.getItem('token')){
            return true;
        } else {
            return false;
        }
    }




}